package com.example.matrimony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.matrimony.R;
import com.example.matrimony.activity.AddCandidateActivity;
import com.example.matrimony.model.UserLanguageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LanguageAdapter extends BaseAdapter {

    Context context;
    ArrayList<UserLanguageModel> userLanguageList;

    public LanguageAdapter(AddCandidateActivity addCandidateActivity, ArrayList<UserLanguageModel> languageList) {

    }

    @Override
    public int getCount() {
        return userLanguageList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        ViewHolder viewHolder;
        if (v==null){
            v = LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) v.getTag();
        }

        viewHolder.tvName.setText(userLanguageList.get(i).getName());

        return v;
    }

    static
    class ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
