package com.example.matrimony.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserLanguageModel implements Serializable {
    int UserLanguageId;
    String name;

    public int getUserLanguageId() {
        return UserLanguageId;
    }

    public void setUserLanguageId(int userLanguageId) {
        UserLanguageId = userLanguageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override public String toString() {
    return "UserLanguageModel{" +
            "UserLanguageId=" + UserLanguageId +
            ", name='" + name + '\'' +
            '}';
}}
